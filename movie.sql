-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1
-- Время создания: Авг 31 2020 г., 05:13
-- Версия сервера: 10.4.14-MariaDB
-- Версия PHP: 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `slim_project`
--

-- --------------------------------------------------------

--
-- Структура таблицы `movie`
--

CREATE TABLE `movie` (
  `id` int(11) NOT NULL,
  `title` varchar(191) NOT NULL,
  `link` varchar(191) NOT NULL,
  `description` text NOT NULL,
  `pub_date` datetime NOT NULL,
  `image` varchar(191) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `movie`
--

INSERT INTO `movie` (`id`, `title`, `link`, `description`, `pub_date`, `image`) VALUES
(1, 'The Pale Door - Clip', 'https://trailers.apple.com/trailers/independent/the-pale-door', 'The Dalton gang finds shelter in a seemingly uninhabited ghost town after a train robbery goes south. Seeking help for their wounded leader, they are surprised to stumble upon a welcoming brothel in the town’s square. But the beautiful women who greet them are actually a coven of witches with very sinister plans for the unsuspecting outlaws - and the battle between good and evil is just beginning.', '2020-08-20 00:00:00', NULL),
(2, 'On The Rocks - Trailer', 'https://trailers.apple.com/trailers/apple/on-the-rocks', 'Faced with sudden doubts about her marriage, a young New York mother (Rashida Jones) teams up with her larger-than-life playboy father (Bill Murray) to tail her husband (Marlon Wayans) in a bittersweet comedy written and directed by Sofia Coppola. In theaters and on Apple TV+ this October.', '2020-08-19 00:00:00', NULL),
(3, 'The Vanished - Trailer', 'https://trailers.apple.com/trailers/independent/the-vanished', 'A family vacation takes a terrifying turn when parents Paul and Wendy discover their young daughter has vanished without a trace. Stopping at nothing to find her, the search for the truth leads to a shocking revelation in this psychological thriller.', '2020-08-18 00:00:00', NULL),
(4, 'The Mole Agent - Trailer', 'https://trailers.apple.com/trailers/independent/the-mole-agent', 'When a family grows concerned for their mother’s well-being in a retirement home, private investigator Romulo hires 83-year-old Sergio to pose as a new resident and undercover spy inside the facility. The Mole Agent follows Sergio as he struggles to balance his assignment with his increasing involvement in the lives of the many residents he meets.', '2020-08-18 00:00:00', NULL),
(5, 'King of the Cruise - Trailer', 'https://trailers.apple.com/trailers/independent/king-of-the-cruise', 'Baron Ronald Busch Reisinger of Inneryne spends his time on outrageous cruise ships amongst romantic couples, wealthy families, hardworking staff and retired elderly. On one such cruise we follow the Baron; parading on the decks in his feudal quilt and king’s cape, he leaves a first impression of being proud and arrogant, bragging about his status, wealth and extravagant life. But there is something underneath his swagger; a universal human desire for recognition and validation.', '2020-08-18 00:00:00', NULL),
(6, 'Sno Babies - Trailer ', 'https://trailers.apple.com/trailers/independent/sno-babies', 'A gripping and emotive tale, Sno Babies depicts the grim realities of addiction and its effects on a middle-class suburban town. Kristen and Hannah are best friends–smart, likable and college-bound– and also addicted to heroin. The pair of seemingly unlikely addicts spiral down a path of destruction, hiding their secret from well-meaning but busy parents behind pink bedrooms and school uniforms. Directed by Bridget Smith and starring Katie Kelly (Game of Silence), Paola Andino (Queen of the South), Michael Lombardi (Rescue Me), Evangeline Young (The Good Wife) and Joanne Baron (This Is Us), Sno Babies shows how easy it can be to both miss and hide the signs of addiction behind the façade of “good” neighborhoods and pleasantly busy communities.', '2020-08-17 00:00:00', NULL),
(7, 'The Secrets We Keep - Trailer', 'https://trailers.apple.com/trailers/independent/the-secrets-we-keep', 'In post-WWII America, a woman [Noomi Rapace], rebuilding her life in the suburbs with her husband [Chris Messina], kidnaps her neighbor [Joel Kinnaman] and seeks vengeance for the heinous war crimes she believes he committed against her. Directed by Yuval Adler from a script by Ryan Covington and Yuval Adler.', '2020-08-17 00:00:00', NULL),
(8, 'Creem: America\'s Only Rock \'N\' Roll Magazine - Clip - Creem Was A Place For Misfits', 'https://trailers.apple.com/trailers/independent/creem-americas-only-rock-n-roll-magazine', 'Capturing the messy upheaval of the \'70s just as rock was re-inventing itself, the film explores CREEM Magazine\'s humble beginnings in post-riot Detroit, follows its upward trajectory from underground paper to national powerhouse, then bears witness to its imminent demise following the tragic and untimely deaths of its visionary publisher, Barry Kramer, and its most famous alum and genius clown prince, Lester Bangs, a year later. Fifty years after publishing its first issue, \"America\'s Only Rock \'n\' Roll Magazine\" remains a seditious spirit in music and culture.', '2020-08-14 00:00:00', NULL),
(9, 'The Silencing - Clip', 'https://trailers.apple.com/trailers/independent/the-silencing', 'A reformed hunter (Nikolaj Coster-Waldau) and a sheriff (Annabelle Wallis) are caught in a deadly game of cat and mouse when they set out to track a killer who may have kidnapped the hunter’s daughter five years ago.', '2020-08-14 00:00:00', NULL),
(10, 'Critical Thinking - Trailer', 'https://trailers.apple.com/trailers/independent/critical-thinking', 'Based on a true story from 1998, five LatinX and Black teenagers from the toughest underserved ghetto in Miami fight their way into the National Chess Championship under the guidance of their unconventional but inspirational teacher.', '2020-08-14 00:00:00', NULL),
(11, 'Major Arcana - Trailer', 'https://trailers.apple.com/trailers/independent/major-arcana', 'A long-troubled itinerant carpenter returns home to small town Vermont and attempts to build a log cabin by hand, hoping to free himself from a cycle of poverty and addiction. But when he reconnects with Sierra, a woman with whom he shares a complicated past, he becomes locked in a desperate struggle between the person he was and the person he hopes to become.', '2020-08-14 00:00:00', NULL),
(12, 'Aggie - Trailer', 'https://trailers.apple.com/trailers/independent/aggie', 'Aggie\' is a feature-length documentary that explores the nexus of art, race, and justice through the story of art collector and philanthropist Agnes \"Aggie\" Gund\'s life. Emmy-nominated director Catherine Gund focuses on her mother\'s journey to give viewers an understanding of the power of art to transform consciousness and inspire social change.', '2020-08-14 00:00:00', NULL),
(13, 'Spy Cat - Trailer', 'https://trailers.apple.com/trailers/independent/spy-cat', 'Marnie, a pampered house cat, trades a life of luxury for a new adventure as a private eye. With help from new friends and her favorite detective TV shows, Marnie sets out to solve a mystery and prove she’s more than just a house cat.', '2020-08-13 00:00:00', NULL),
(14, 'Spree - Clip', 'https://trailers.apple.com/trailers/independent/spree', 'Meet Kurt (Joe Keery), a 23-year-old rideshare driver for Spree, who is so desperate for social media attention that he\'ll stop at nothing to go viral. He comes up with a plan to livestream a rampage as a shortcut to infamy - coining his evil scheme \"#thelesson\", he installs a set of cameras in his car and begins streaming his rides. Wildly miscalculating the popularity that would come from his lethal scheme, Kurt’s desperation grows as he tries to find a way to overcome the plan\'s flaws. In the middle of all this madness, a stand-up comedian (Sasheer Zamata) with her own viral agenda crosses Kurt\'s path and becomes the only hope to put a stop to his misguided carnage.', '2020-08-13 00:00:00', NULL),
(15, 'Endless - Clip', 'https://trailers.apple.com/trailers/independent/endless', 'Endless follows love struck high school graduates Riley (Alexandra Shipp) and Chris (Nicholas Hamilton). When they are separated by a tragic car accident, Riley blames herself for her boyfriend’s death while Chris is stranded in limbo. Miraculously, the two find a way to connect. In a love story that transcends life and death, both Riley and Chris are forced to learn the hardest lesson of all: letting go.', '2020-08-13 00:00:00', NULL),
(16, 'Helmut Newton: The Bad And The Beautiful - Trailer', 'https://trailers.apple.com/trailers/independent/helmut-newton-the-bad-and-the-beautiful', 'One of the great masters of photography, Helmut Newton made a name for himself exploring the female form, and his cult status continues long after his tragic death in a Los Angeles car crash in 2004. Newton worked around the globe, from Singapore to Australia to Paris to Los Angeles, but Weimar Germany was the visual hallmark of his work. Newton\'s unique and striking way of depicting women has always posed the question: did he empower his subjects or treat them as sexual objects? Through candid interviews with Grace Jones, Charlotte Rampling, Isabella Rossellini, Anna Wintour, Claudia Schiffer, Marianne Faithfull, Hanna Schygulla, Nadja Auermann, and Newton\'s wife June (a.k.a. photographer Alice Springs), this documentary captures his legacy and seeks to answer questions about the themes at the core of his life\'s work – creating provocative and subversive images of women. The film also features Newton’s own home movies, archival footage (including a pointed exchange with Susan Sontag) and, of course, scores of iconic Newton photographs. The result: a wildly entertaining portrait of a controversial genius.', '2020-08-13 00:00:00', NULL),
(17, 'Find Your Voice - Trailer', 'https://trailers.apple.com/trailers/independent/find-your-voice', 'E wants to write a hit song, but he is a rapper with nothing original to say. Roaming the streets of Sydney, he searches for the meaning of life, running into some seedy characters while trying to find himself. He ends up going on an adventure to learn about his cultural roots but meets a mysterious girl who upends his life so that E must hit rock bottom before he can find his voice.', '2020-08-13 00:00:00', NULL),
(18, 'Martin Margiela: In His Own Words - Trailer ', 'https://trailers.apple.com/trailers/oscilloscope/martin-margiela-in-his-own-words', 'One of the most revolutionary and influential fashion designers of his time, Martin Margiela has remained an elusive figure the entirety of his decades-long career. From Jean Paul Gaultier’s assistant to creative director at Hermès to leading his own House, Margiela never showed his face publicly and avoided interviews, but reinvented fashion with his radical style through forty-one provocative collections. Now, for the first time, the “Banksy of fashion” reveals his drawings, notes, and personal items in this exclusive, intimate profile of his vision.', '2020-08-13 00:00:00', NULL),
(19, 'The Owners - Trailer', 'https://trailers.apple.com/trailers/independent/the-owners', 'A group of friends think they found the perfect easy score - an empty house with a safe full of cash. But when the elderly couple that lives there comes home early the tables are suddenly turned. As a deadly game of cat and mouse ensues the would-be thieves are left to fight to save themselves from a nightmare they could never have imagined.', '2020-08-13 00:00:00', NULL),
(20, 'Happy Happy Joy Joy: The Ren & Stimpy Story - Trailer', 'https://trailers.apple.com/trailers/independent/happy-happy-joy-joy-the-ren-stimpy-story', 'In the early 1990s, the animated show Ren & Stimpy broke ratings records and was a touchstone for a generation of fans and artists. Creator John Kricfalusi was celebrated as a visionary, but even though his personality suffused the show, dozens of artists and network executives were just as responsible for the show’s meteoric rise. As Kricfalusi’s worst impulses were let loose at the workplace and new allegations about even more disturbing behavior have surfaced, his reputation now threatens to taint the show forever. With clips recognizable to any Ren & Stimpy fan and interviews with Kricfalusi and his fellow creators whose work has been both elevated and denigrated by their connection to him, this film is a complex look at a show that influenced the history of television, animation, and comedy.', '2020-08-13 00:00:00', NULL);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `movie`
--
ALTER TABLE `movie`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `movie`
--
ALTER TABLE `movie`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
